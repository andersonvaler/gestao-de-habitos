import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import React from "react";
import { Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: 250,
  },
}));

const GroupGoalsEdit = ({ elm, setOpenEdit }) => {
  const { token } = useToken();
  const { user, loadGroup } = useUser();
  const classes = useStyles();
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
    how_much_achieved: yup.number().required("Campo Obrigatório").integer(),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const deleteGoals = (id) => {
    axios
      .delete(`https://kabit-api.herokuapp.com/goals/${id}/`, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
      })
      .catch((error) => console.log(error));
    back();
  };

  const handleForm = (data) => {
    let bool_achieved = false;
    data.how_much_achieved < 100
      ? (bool_achieved = false)
      : (bool_achieved = true);
    data = { ...data, achieved: bool_achieved, group: user.group };

    axios
      .patch(`https://kabit-api.herokuapp.com/goals/${elm.id}/`, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        setOpenEdit(false);
        loadGroup();
      })
      .catch((error) => console.log(error));
  };

  const back = () => {
    setOpenEdit(false);
  };

  const [input, setInput] = useState({
    title: elm.title,
    difficulty: elm.difficulty,
    how_much_achieved: elm.how_much_achieved,
  });

  const handleChange = (e) => {
    switch (e.target.name) {
      case "title":
        setInput({ ...input, title: e.target.value });
        break;
      case "difficulty":
        setInput({ ...input, difficulty: e.target.value });
        break;
      case "achieved":
        setInput({ ...input, achieved: e.target.value });
        break;
      case "how_much_achieved":
        if (e.target.value > 100) {
          e.target.value = 100;
        }
        if (e.target.value < 0) {
          e.target.value = 0;
        }
        setInput({ ...input, how_much_achieved: Number(e.target.value) });
        break;
      default:
        break;
    }
  };

  return (
    <div className="goals-edit-container">
      <h1> Editar Meta</h1>
      <form onSubmit={handleSubmit(handleForm)}>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Título"
            name="title"
            size="small"
            color="primary"
            className={classes.textField}
            value={input.title}
            onChange={handleChange}
            inputRef={register}
            error={!!errors.title}
            helperText={errors.title?.message}
          />
        </div>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Dificuldade"
            name="difficulty"
            size="small"
            color="primary"
            className={classes.textField}
            value={input.difficulty}
            inputRef={register}
            onChange={handleChange}
            error={!!errors.difficulty}
            helperText={errors.difficulty?.message}
          />
        </div>
        <div>
          <TextField
            type="number"
            margin="normal"
            variant="outlined"
            label="Status"
            name="how_much_achieved"
            size="small"
            color="primary"
            className={classes.textField}
            value={input.how_much_achieved}
            inputRef={register}
            onChange={handleChange}
            error={!!errors.how_much_achieved}
            helperText={errors.how_much_achieved?.message}
          />
        </div>
        <div className="buttons-edit-goals">
          <Button type="submit" color="primary">
            <SaveIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => deleteGoals(elm.id)}>
            <DeleteIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => back()}>
            <ExitToAppIcon fontSize="large" />
          </Button>
        </div>
      </form>
    </div>
  );
};

export default GroupGoalsEdit;
