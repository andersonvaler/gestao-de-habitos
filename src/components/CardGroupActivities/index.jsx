import { useUser } from "../../Providers/User";
import { useState } from "react";
import FormActivitiesEdit from "../FormActivitiesEdit";
import FormActivitiesCreate from "../FormActivitiesCreate";
import { Card } from "@material-ui/core";
import AddCircleRoundedIcon from "@material-ui/icons/AddCircleRounded";
import MenuOpenOutlinedIcon from "@material-ui/icons/MenuOpenOutlined";
import ModalCard from "../ModalCard";
import "./cardGroupStyles.css";
import "../../styles/global";

const CardGroupActivities = () => {
  const { groupUser } = useUser();
  const [openEdit, setOpenEdit] = useState(false);
  const [openNew, setOpenNew] = useState(false);
  const [activitiesEdit, setActivitiesEdit] = useState("");
  const [activitiesCreate, setActivitiesCreate] = useState(false);
  const showActivitiesDetails = (elm) => {
    setActivitiesEdit(elm);
    setOpenEdit(true);
  };

  const newActivities = () => {
    setOpenNew(true);
    setActivitiesCreate(true);
  };

  return (
    <div className="habits-container">
      <h2>Atividades do Grupo</h2>
      <div>
        {groupUser &&
          groupUser.activities.map((elm, index) => {
            const filter = elm.realization_time;
            const filterTime = `${filter.substr(8, 2)}${filter.substr(
              4,
              4
            )}${filter.substr(0, 4)} ${filter.substr(11, 5)}`;
            return (
              <Card className="cardActivities" key={index}>
                <h1
                  onClick={() => showActivitiesDetails(elm)}
                  className="cardTitle"
                >
                  {elm.title}
                </h1>
                <span className="flexActivities">
                  <div>{filterTime}</div>
                  <MenuOpenOutlinedIcon
                    onClick={() => showActivitiesDetails(elm)}
                    className="newIcon"
                    color="primary"
                  />
                </span>
              </Card>
            );
          })}
        <Card className="newActivities">
          <span className="flexActivities">
            <h1 onClick={newActivities} className="newIcon">
              Adicionar atividade
            </h1>
            <AddCircleRoundedIcon onClick={newActivities} className="newIcon" />
          </span>
        </Card>
        {activitiesEdit && (
          <ModalCard open={openEdit} setOpen={setOpenEdit}>
            <FormActivitiesEdit
              elm={activitiesEdit}
              setOpenEdit={setOpenEdit}
            />
          </ModalCard>
        )}
        {activitiesCreate && (
          <ModalCard open={openNew} setOpen={setOpenNew}>
            <FormActivitiesCreate setOpenNew={setOpenNew} color="secondary" />
          </ModalCard>
        )}
      </div>
    </div>
  );
};

export default CardGroupActivities;
