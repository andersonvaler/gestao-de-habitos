import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: 250,
  },
}));

const GroupGoalsCreate = ({ setOpenNew }) => {
  const { token } = useToken();
  const { user, loadGroup } = useUser();
  const classes = useStyles();
  const url = "https://kabit-api.herokuapp.com/goals/";
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (_data) => {
    const data = {
      ..._data,
      group: user.group,
      achieved: false,
      how_much_achieved: 0,
    };
    axios
      .post(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
        reset();
        setOpenNew(false);
      })
      .catch((error) => console.log(error));
  };

  const back = () => {
    setOpenNew(false);
  };

  return (
    <div className="create-goal">
      <h1>Criar Meta</h1>
      <form onSubmit={handleSubmit(handleForm)}>
        <TextField
          margin="normal"
          variant="outlined"
          label="Meta"
          name="title"
          size="small"
          color="primary"
          className={classes.textField}
          inputRef={register}
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <br />
        <TextField
          margin="normal"
          variant="outlined"
          label="Dificuldade"
          name="difficulty"
          size="small"
          color="primary"
          className={classes.textField}
          inputRef={register}
          error={!!errors.difficulty}
          helperText={errors.difficulty?.message}
        />
        <br />
        <div className="buttons-goal">
          <Button type="submit" color="primary">
            <SaveIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => back()}>
            <ExitToAppIcon fontSize="large" />
          </Button>
        </div>
      </form>
    </div>
  );
};

export default GroupGoalsCreate;
