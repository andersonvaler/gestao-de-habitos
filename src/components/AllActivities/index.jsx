import { useEffect, useState } from "react";

import axios from "axios";

const AllActivities = () => {
  const [listActivities, setListActivities] = useState();
  const [url, setUrl] = useState("https://kabit-api.herokuapp.com/activities/");

  useEffect(() => {
    axios.get(url).then((response) => {
      setListActivities(response.data);
    });
  }, [url]);

  return (
    <div>
      <h1>Todas Atividades</h1>
      <div>
        {listActivities &&
          listActivities.results.map((elm, index) => {
            return (
              <div key={index}>
                <h2>Titulo: {elm.title}</h2>
                <p>Grupo: {elm.group}</p>
                <p>Data de realizacao: {elm.realization_time}</p>
                <br />
              </div>
            );
          })}
      </div>
      {listActivities && listActivities.previous && (
        <button onClick={() => setUrl(listActivities.previous)}>
          Anterior
        </button>
      )}
      {listActivities && listActivities.next && (
        <button onClick={() => setUrl(listActivities.next)}>Próximo</button>
      )}
    </div>
  );
};

export default AllActivities;
