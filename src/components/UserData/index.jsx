import { useUser } from "../../Providers/User";
import { Card } from "@material-ui/core";

const UserData = () => {
  const { user } = useUser();

  return (
    <div>
      {user && (
        <div className="home-container">
          <Card className="home-card-container">
            <h3>
              <p className="home-username"> {user.username.toUpperCase()}</p>
              Bem vindo a Virtuose!
            </h3>
            <p>Lembre-se, a prática leva a perfeição.</p>
          </Card>
        </div>
      )}
      {!user && (
        <div className="home-container">
          <Card className="home-card-container">
            <h3>
              <p className="home-username"> Caro usuário</p>
              Carregando a página...
            </h3>
            <p> Verifique sua conexão se a demora persistir!</p>
          </Card>
        </div>
      )}
    </div>
  );
};

export default UserData;
