import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import React from "react";
import { Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: 250,
  },
}));

const FormActivitiesEdit = ({ elm, setOpenEdit }) => {
  const { token } = useToken();
  const { user, loadGroup } = useUser();
  const classes = useStyles();

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    realization_time: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const deleteActivities = (id) => {
    axios
      .delete(`https://kabit-api.herokuapp.com/activities/${id}/`, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
      })
      .catch((error) => console.log(error));
    back();
  };

  const handleForm = (data) => {
    data.group = user.group;
    axios
      .patch(`https://kabit-api.herokuapp.com/activities/${elm.id}/`, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
        back();
      })
      .catch((error) => console.log(error));
  };

  const back = () => {
    setOpenEdit(false);
  };

  const [input, setInput] = useState({
    title: elm.title,
    realization_time: elm.realization_time,
  });

  const handleChange = (e) => {
    switch (e.target.name) {
      case "title":
        setInput({ ...input, title: e.target.value });
        break;
      case "realization_time":
        setInput({ ...input, realization_time: e.target.value });
        break;
      default:
        break;
    }
  };

  return (
    <div className="edit-activities">
      <h1> Editar Atividade</h1>
      <form onSubmit={handleSubmit(handleForm)} noValidate>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Atividade"
            name="title"
            size="small"
            color="primary"
            className={classes.textField}
            value={input.title}
            onChange={handleChange}
            inputRef={register}
            error={!!errors.title}
            helperText={errors.title?.message}
          />
        </div>
        <div>
          <TextField
            id="datetime-local"
            margin="normal"
            variant="outlined"
            size="small"
            color="primary"
            label="Data de realização"
            name="realization_time"
            type="datetime-local"
            onChange={handleChange}
            defaultValue={elm.realization_time.substr(0, 16)}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            inputRef={register}
            error={!!errors.realization_time}
            helperText={errors.realization_time?.message}
          />
        </div>

        <div className="buttons-activity">
          <Button type="submit" color="primary" onClick={() => {}}>
            <SaveIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => deleteActivities(elm.id)}>
            <DeleteIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => back()}>
            <ExitToAppIcon fontSize="large" />
          </Button>
        </div>
      </form>
    </div>
  );
};

export default FormActivitiesEdit;
