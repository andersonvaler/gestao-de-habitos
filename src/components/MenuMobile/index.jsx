import React from "react";
import { useHistory } from "react-router-dom";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import HomeIcon from "@material-ui/icons/Home";
import GroupIcon from "@material-ui/icons/Group";
import TrackChangesIcon from "@material-ui/icons/TrackChanges";
import InfoIcon from "@material-ui/icons/Info";
import "./MenuMobile.css";

const MenuMobile = () => {
  const [value, setValue] = React.useState("recents");
  const history = useHistory();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="container-mobile">
      <BottomNavigation
        className="menu-mobile-container"
        value={value}
        onChange={handleChange}
      >
        <BottomNavigationAction
          value="home"
          icon={<HomeIcon fontSize="large" />}
          onClick={() => history.push("/home")}
        />
        <BottomNavigationAction
          value="groups"
          icon={<GroupIcon fontSize="large" />}
          onClick={() => history.push("/group")}
        />
        <BottomNavigationAction
          value="goals"
          icon={<TrackChangesIcon fontSize="large" />}
          onClick={() => history.push("/habit")}
        />
        <BottomNavigationAction
          value="menu"
          icon={<InfoIcon fontSize="large" />}
          onClick={() => history.push("/user")}
        />
      </BottomNavigation>
    </div>
  );
};
export default MenuMobile;
