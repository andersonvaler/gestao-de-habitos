import { useGroups } from "../../Providers/Groups";
import axios from "axios";
import { useEffect, useState } from "react";
import { useToken } from "../../Providers/Token";
import "./listGroups.css";
import { useUser } from "../../Providers/User";
import { useHistory } from "react-router";
// import { GroupAdd } from "@material-ui/icons";

const ListGroups = () => {
  const history = useHistory();
  const { loadUser } = useUser();
  const { groups, setGroups } = useGroups();
  const { token } = useToken();
  const [url, setUrl] = useState(`https://kabit-api.herokuapp.com/groups/`);

  const handlePage = (instruction) => {
    if (instruction === "next") {
      setUrl(groups.next);
    }
    if (instruction === "previous") {
      setUrl(groups.previous);
    }
    document.documentElement.scrollTop = 0;
  };

  useEffect(() => {
    axios
      .get(url)
      .then((response) => {
        setGroups(response.data);
      })
      .catch((e) => console.log(e));
    // eslint-disable-next-line
  }, [url]);

  const subscribe = (id) => {
    const api = `https://kabit-api.herokuapp.com/groups/${id}/subscribe/`;
    axios
      .post(
        api,
        {},
        { headers: { Authorization: `Bearer ${JSON.parse(token)}` } }
      )
      .then(() => {
        loadUser();
        history.push("/");
      });
  };

  return (
    <>
      <div className="groups-container">
        <br />
        {groups &&
          groups.results.map((group, index) => {
            return (
              <div className="group-card" key={index}>
                <div className="group">
                  <h2>{group.name}</h2>
                  <p>
                    <span>Descrição: </span>
                    <section>{group.description}</section>
                  </p>
                  <p>
                    <span>Categoria:</span> <section>{group.category}</section>
                  </p>
                  <p>
                    <span>Membros:</span>{" "}
                    <section>{group.users.length}</section>
                  </p>
                  <p>
                    <span>Atividades:</span>{" "}
                    <section>{group.activities.length}</section>
                  </p>
                  <p>
                    <span>Metas:</span> <section>{group.goals.length}</section>
                  </p>
                </div>
                <button onClick={() => subscribe(group.id)}>Subscribe</button>
                <br />
                <br />
              </div>
            );
          })}
        <br />
      </div>
      <div className="groups-pagination">
        {groups && groups.previous && (
          <button onClick={() => handlePage("previous")}>Anterior</button>
        )}
        {groups && groups.next && (
          <button onClick={() => handlePage("next")}>Próxima</button>
        )}
      </div>
    </>
  );
};

export default ListGroups;
