import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useState } from "react";
import axios from "axios";
import "./formRegister.css";
import { createMuiTheme, styled } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import MuiAlert from "@material-ui/lab/Alert";
import { Button, TextField } from "@material-ui/core";

const Text = styled(TextField)({
  margin: "5px",
  width: "350px",
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const FormRegister = () => {
  const history = useHistory();
  const [error, setError] = useState(false);
  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    username: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 dígitos")
      .required("Campo obrigatório"),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref("password"), null], "Senhas não coincidem!")
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    delete data.confirmPassword;
    axios
      .post("https://kabit-api.herokuapp.com/users/", data)
      .then((response) => {
        reset();
        history.push("/");
      })
      .catch((error) => {
        if (error.response.status === 400) {
          setError("Este nome de usuário já existe!");
          reset();
        }
      });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#6325a0",
      },
      secondary: {
        main: "#3BA597",
      },
      error: {
        main: "#FF220C",
      },
    },
  });

  return (
    <div className="register-container">
      <h2>Bem vindo(a) ao Virtuose</h2>
      <h4>Está na hora de não deixar para amanhã!</h4>

      <form onSubmit={handleSubmit(handleForm)}>
        <ThemeProvider theme={theme}>
          <Text
            margin="normal"
            variant="outlined"
            label="Email"
            name="email"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
          />

          <Text
            autoComplete="username"
            margin="normal"
            variant="outlined"
            label="Username"
            name="username"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.username}
            helperText={errors.username?.message}
          />

          <Text
            autoComplete="current-password"
            type="password"
            margin="normal"
            variant="outlined"
            label="Senha"
            name="password"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
          />

          <Text
            autoComplete="current-password"
            type="password"
            margin="normal"
            variant="outlined"
            label="Confirmação de Senha"
            name="confirmPassword"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.confirmPassword}
            helperText={errors.confirmPassword?.message}
          />

          <div className="buttons">
            <Button type="submit" variant="contained" color="secondary">
              Cadastrar Usuário
            </Button>
          </div>
        </ThemeProvider>
      </form>
      {error && (
        <Alert severity="warning" className="alert">
          {error}
        </Alert>
      )}
    </div>
  );
};
export default FormRegister;
