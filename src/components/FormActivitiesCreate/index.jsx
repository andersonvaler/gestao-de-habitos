import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import React from "react";
import { Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: 250,
  },
}));

const FormActivitiesCreate = ({ setOpenNew }) => {
  const { token } = useToken();
  const { user, loadGroup } = useUser();
  const classes = useStyles();

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    realization_time: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    data.group = user.group;
    axios
      .post("https://kabit-api.herokuapp.com/activities/", data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
        reset();
        setOpenNew(false);
      })
      .catch((error) => console.log(error));
  };

  const back = () => {
    setOpenNew(false);
  };

  return (
    <div className="new-activity">
      <h1> Criar Atividade</h1>
      <form onSubmit={handleSubmit(handleForm)} noValidate>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Atividade"
            name="title"
            size="small"
            color="primary"
            className={classes.textField}
            inputRef={register}
            error={!!errors.title}
            helperText={errors.title?.message}
          />
        </div>
        <div>
          <TextField
            id="datetime-local"
            margin="normal"
            variant="outlined"
            size="small"
            color="primary"
            label="Data de realização"
            name="realization_time"
            type="datetime-local"
            defaultValue="2021-01-01T12:00"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            inputRef={register}
            error={!!errors.realization_time}
            helperText={errors.realization_time?.message}
          />
        </div>
        <div className="buttons-activity">
          <Button type="submit" color="primary">
            <SaveIcon fontSize="large" />
          </Button>
          <Button color="primary" onClick={() => back()}>
            <ExitToAppIcon fontSize="large" />
          </Button>
        </div>
      </form>
    </div>
  );
};

export default FormActivitiesCreate;
