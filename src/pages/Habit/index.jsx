import CardCheckHabit from "../../components/CardCheckHabit";
import Header from "../../components/Header";
import MenuMobile from "../../components/MenuMobile";
import "./style.css";

const Habit = () => {
  return (
    <div>
      <Header />
      <CardCheckHabit />
      <MenuMobile />
    </div>
  );
};

export default Habit;
