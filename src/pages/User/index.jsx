import Info from "../../components/Info";
import Header from "../../components/Header";
import MenuMobile from "../../components/MenuMobile";

const User = () => {

 
  return (
    <div>
      <Header />
      <Info />
      <MenuMobile />
    </div>
  );
};

export default User;
