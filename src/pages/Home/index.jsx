import { useEffect } from "react";
import { useUser } from "../../Providers/User";
import { useToken } from "../../Providers/Token";
import Header from "../../components/Header";
import MenuMobile from "../../components/MenuMobile";
import UserData from "../../components/UserData";
import "./homeStyle.css";
import PersonalGroup from "../../components/PersonalGroup";
import CardCheckHabit from "../../components/CardCheckHabit";

const Home = () => {
  const { token } = useToken();
  const { loadUser, loadGroup } = useUser();

  useEffect(() => {
    token && loadUser();
    loadGroup();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Header />
      <UserData />
      <div className="personal-group-container">
        <CardCheckHabit />
      </div>
      <PersonalGroup />
      <MenuMobile />
    </>
  );
};

export default Home;
