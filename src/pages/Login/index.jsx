import FormLogin from "../../components/FormLogin";
import Header from "../../components/Header";

const Login = () => {
  return (
    <div>
      <Header />
      <FormLogin />
    </div>
  );
};

export default Login;
