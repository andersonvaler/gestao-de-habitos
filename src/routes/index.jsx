import Login from "../pages/Login";
import Home from "../pages/Home";
import Register from "../pages/Register";
import User from "../pages/User";
import Habit from "../pages/Habit";
import Group from "../pages/Group";
import { useToken } from "../Providers/Token";
import { Switch, Route, Redirect } from "react-router-dom";

const Routes = () => {
  const { token } = useToken();
  const currentToken = JSON.parse(token);
  return (
    <Switch>
      <Route path="/home">
        {currentToken ? <Home /> : <Redirect to="/" />}
      </Route>
      <Route path="/user">
        {currentToken ? <User /> : <Redirect to="/" />}
      </Route>
      <Route exact path="/">
        {currentToken ? <Redirect to="/home" /> : <Login />}
      </Route>
      <Route path="/register">
        <Register />
      </Route>
      <Route exact path="/habit">
        {currentToken ? <Habit /> : <Redirect to="/" />}
      </Route>
      <Route exact path="/group">
        {currentToken ? <Group /> : <Redirect to="/" />}
      </Route>
    </Switch>
  );
};

export default Routes;
