import { createContext, useContext, useState, useEffect } from "react";

// import { useKeepLogin } from "../KeepLogin/index";

const TokenContext = createContext();

export const TokenProvider = ({ children }) => {
  // const { setStorage } = useKeepLogin();

  const [token, setToken] = useState(localStorage.getItem("token") || false);

  useEffect(() => {
    localStorage.setItem("token", token);
  }, [token]);

  return (
    <TokenContext.Provider value={{ token, setToken }}>
      {children}
    </TokenContext.Provider>
  );
};

export const useToken = () => useContext(TokenContext);
