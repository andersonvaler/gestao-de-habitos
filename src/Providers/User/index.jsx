import { createContext, useContext, useState } from "react";
import jwt_decode from "jwt-decode";
import axios from "axios";
import { useToken } from "../../Providers/Token";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState();
  const [groupUser, setGroupUser] = useState();
  const { token } = useToken();

  const loadUser = () => {
    const { user_id } = jwt_decode(JSON.parse(token));
    axios
      .get(`https://kabit-api.herokuapp.com/users/${user_id}/`)
      .then((response) => {
        setUser(response.data);
      })
      .catch((e) => console.log(e));
  };

  const loadGroup = () => {
    user &&
      user.group &&
      axios
        .get(`https://kabit-api.herokuapp.com/groups/${user.group}/`)
        .then((response) => {
          setGroupUser(response.data);
        })
        .catch((error) => console.log(error));
  };

  return (
    <UserContext.Provider
      value={{ user, setUser, loadUser, groupUser, loadGroup, setGroupUser }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
