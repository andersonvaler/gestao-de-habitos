# Vituose - Gestão de Hábitos

Projeto desenvolvido pelo grupo 4 da turma de outubro-2020, facilitador Willian Brusch

## Padronização do projeto

Alguns padrões a serem seguidos pelo time de desenvolvimento

### Commits

Utilizar os padrões Conventional Commits 1.0.0, mas para a descrição escrever em Português.\
Acesse => [Conventional Commits.](https://www.conventionalcommits.org/en/v1.0.0/)

### Branches

Utilizar o padrão GitFlow, link abaixo, e criar branches por funcionalidade.\
Acesse => [Aula GitFlow no Canvas.](https://alunos.kenzie.com.br/courses/28/assignments/4266?module_item_id=4801)

### Trello

Acesse => [Link para o Trello.](https://trello.com/b/7Ja4qgrZ/grupo-4)

### Whimsical - Fluxograma, Layout e Informações adicionais

Acesse => [Link para o grupo no Whimsical.](https://whimsical.com/go-collaborate-XLZH6Qe2jnj3drKhCEpaaX)
